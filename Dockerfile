FROM ubuntu:18.04

LABEL maintainer="Colin Wilson (colin@aig.is)"
LABEL vendor="AIGIS Services Ltd"

ARG GEOIPUPDATE_VERSION=4.0.3

# install required packages
RUN apt-get update -y && \
    apt-get install -y curl cron && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Download & install MaxMind geoipupdate
RUN curl -o /home/geoipupdate_${GEOIPUPDATE_VERSION}_linux_amd64.deb -L https://github.com/maxmind/geoipupdate/releases/download/v${GEOIPUPDATE_VERSION}/geoipupdate_${GEOIPUPDATE_VERSION}_linux_amd64.deb
RUN cd /home && dpkg -i geoipupdate_${GEOIPUPDATE_VERSION}_linux_amd64.deb

# Add MaxMind free GeoLite2 database configuration
ADD GeoIP.conf /etc/GeoIP.conf

# Run GeoIP Update
RUN geoipupdate

# Set cronjob (Every Wednesday @ 12:00) for geoipupdate
RUN (crontab -l; echo "0 12 * * 3 geoipupdate >> /dev/stdout") | crontab

# Run cron
CMD ["cron", "-f"]